/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Student
 */
import java.sql.*;
import java.util.*;

/**
 * DatabaseUtils
 * User: Michael
 * Date: Aug 17, 2010
 * Time: 7:58:02 PM
 */
//shamelessly borrowed and modified from an online exmple
public class DatabaseUtils
{
    private static final String DEFAULT_DRIVER = "org.apache.derby.jdbc.ClientDriver";
    private static final String DEFAULT_URL = "jdbc:derby://localhost:1527/vilham2";
    private static final String DEFAULT_USERNAME = "vilham2";
    private static final String DEFAULT_PASSWORD = "";

    public static Connection createDefaultConnection() throws ClassNotFoundException, SQLException
    {
        return createConnection(DEFAULT_DRIVER, DEFAULT_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD);
    }

    public static Connection createConnection(String driver, String url, String username, String password) throws ClassNotFoundException, SQLException
    {
        Class.forName(driver);

        if ((username == null) || (password == null) || (username.trim().length() == 0) || (password.trim().length() == 0))
        {
            return DriverManager.getConnection(url);
        }
        else
        {
            return DriverManager.getConnection(url, username, password);
        }
    }

    public static void close(Connection connection)
    {
        try
        {
            if (connection != null)
            {
                connection.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


    public static void close(Statement st)
    {
        try
        {
            if (st != null)
            {
                st.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static void close(ResultSet rs)
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static void rollback(Connection connection)
    {
        try
        {
            if (connection != null)
            {
                connection.rollback();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static List<Map<String, Object>> map(ResultSet rs) throws SQLException {
        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
        try {
            if (rs != null) {
                ResultSetMetaData meta = rs.getMetaData();
                int numColumns = meta.getColumnCount();
                while (rs.next()) {
                    Map<String, Object> row = new HashMap<String, Object>();
                    for (int i = 1; i <= numColumns; ++i) {
                        String name = meta.getColumnName(i);
                        Object value = rs.getObject(i);
                        row.put(name, value);
                    }
                    results.add(row);
                }
            }
        } finally {
            close(rs);
        }
        return results;
    }

    public static List<Map<String, Object>> query(Connection connection, String sql, List<Object> parameters) throws SQLException
    {

        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Map<String, Object>> returnValue;

        try
        {
            ps = connection.prepareStatement(sql);

            int i = 0;
            for (Object parameter : parameters)
            {
                ps.setObject(++i, parameter);
            }

            rs = ps.executeQuery();
            returnValue = map(rs);
        }
        finally
        {
            close(rs);
            close(ps);
        }

        return returnValue;
    }

    public static int update(Connection connection, String sql, List<Object> parameters) throws SQLException
    {
        int numRowsUpdated = 0;

        PreparedStatement ps = null;

        try
        {
            ps = connection.prepareStatement(sql);

            int i = 0;
            for (Object parameter : parameters)
            {
                ps.setObject(++i, parameter);
            }

            numRowsUpdated = ps.executeUpdate();
        }
        finally
        {
            close(ps);
        }

        return numRowsUpdated;
    }
    
    public static int createSubmission(Connection connection, List<Object> parameters) throws SQLException{
        String sql = "INSERT INTO APP.SUBMISSIONS VALUES (?, ?, ?)";
        return update(connection, sql, parameters);
    }
    
    public static int createPayouts(Connection connection, List<Object> parameters, int userCount) throws SQLException{
        if(userCount > 0){
            String sql = "INSERT INTO APP.PAYOUTS VALUES (?, ?, ?)";
            for(int i = 1; i < userCount; i++){
                sql += ",(?, ?, ?)";
            }
            return update(connection, sql, parameters);
        }
        return 0;
    }
    
    public static List<Map<String, Object>> getSubmissionInformation(Connection connection, List<Object> parameters) throws SQLException{
        String sql = "SELECT TAX_RATE, LOOT_TOTAL FROM APP.SUBMISSIONS WHERE TRANSACTION_ID = ?";
        return DatabaseUtils.query(connection, sql, parameters);
    }
}