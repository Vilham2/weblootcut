/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student
 */
public class TransactionDisplay extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String parameter = request.getParameter("transactionID");
        long transactionID = Long.parseLong(parameter);
        double lootTotal = 0;
        double taxRate = 0;
        double corpCut = 0;
        List<String> characterNames = null;
        List<Double> shares = null;
        List<Double> lootCut = null;
        
        Connection connection = null;
        List<Map<String, Object>> queryResults = null;
        List<Object> parameters = new ArrayList<Object>();
        
        //fetch and parse general transaction information
        try{
            connection = DatabaseUtils.createDefaultConnection();
            parameters.add(transactionID);
            queryResults = DatabaseUtils.getSubmissionInformation(connection, parameters);
            for(int i = 0; i < queryResults.size(); i++){
                Map<String, Object> map = queryResults.get(i);
                for ( Map.Entry<String, Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                    Object object = entry.getValue();
                    if(key.equalsIgnoreCase("LOOT_TOTAL")){
                        lootTotal = Double.parseDouble(object.toString());
                    }else if(key.equalsIgnoreCase("TAX_RATE")){
                        taxRate = Double.parseDouble(object.toString());
                    }
                }
            }
            corpCut = (taxRate/100) * lootTotal;
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            DatabaseUtils.close(connection);
        }
                
        //fetch and parse individual payout details
        try{
            connection = DatabaseUtils.createDefaultConnection();
            String sql = "SELECT * FROM APP.PAYOUTS WHERE TRANSACTION_ID = ?";
            queryResults = DatabaseUtils.query(connection, sql, parameters);
            
            characterNames = new ArrayList<String>();
            shares = new ArrayList<Double>();
            lootCut = new ArrayList<Double>();
            
            for(int i = 0; i < queryResults.size(); i++){
                Map<String, Object> map = queryResults.get(i);
                for ( Map.Entry<String, Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                    Object object = entry.getValue();

                    if(key.equalsIgnoreCase("USERNAME")){
                        characterNames.add(object.toString());
                    }else if(key.equalsIgnoreCase("SHARE_COUNT")){
                        shares.add(Double.parseDouble(object.toString()));
                    }
                }
            }
            
            double sharesTotal = 0;
            for(int i = 0; i < shares.size(); i++){
                sharesTotal += shares.get(i);
            }
            double lootPerShare = ((100-taxRate)/100) * lootTotal;
            
            for(int i = 0; i < shares.size(); i++){
                lootCut.add((lootPerShare * shares.get(i))/sharesTotal);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        constructPage(response, shares, characterNames, lootCut, lootTotal, taxRate, corpCut, transactionID);
    }
    
    //build the html and javascript portions of the page
    private void constructPage(HttpServletResponse response, List<Double> shares, List<String> characterNames, List<Double> lootCut, double lootTotal, double taxRate, double corpCut, long transactionID){
        response.setContentType("text/html;charset=UTF-8");
        
        String outputString = "<p><pre>";
        for(int i = 0; i < shares.size(); i++){
            outputString += "User: " + characterNames.get(i) + " Shares:" + shares.get(i) + " Cut:" + round(lootCut.get(i), 2) + "\n";
        }
        outputString+="</pre></p>";
        try(PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Transaction " + transactionID + "</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Transaction details</h1>");
            out.println("<p>" + lootTotal + " is loot total</p>");
            out.println("<p>" + taxRate + " is tax Rate</p>");
            out.println("<p>" + corpCut + " is corp Cut</p>");
            out.println("<p>Individual Payouts");
            out.println(outputString);
            out.println("</p></body>");
            out.println("</html>");
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
}

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
