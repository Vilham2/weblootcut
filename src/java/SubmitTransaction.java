/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student
 */
public class SubmitTransaction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String lootTotal = request.getParameter("lootTotal");
        String taxRate = request.getParameter("taxRate");
        String corpCut = request.getParameter("corpCut");
                
        String[] characterNames = request.getParameterValues("characterName");
        String[] shares = request.getParameterValues("shares");
        String[] lootCut = request.getParameterValues("lootCut");
        
        long transactionID = System.currentTimeMillis();
        
        
        Connection dbConnection;
        try{
            //dbConnection = DatabaseUtils.createDefaultConnection();
            dbConnection = DatabaseUtils.createDefaultConnection();
            try{
                //fill the parameters ArrayList with the variables for the submission table
                ArrayList<Object> parameters = new ArrayList<Object>();
                parameters.add(transactionID);
                parameters.add(taxRate);
                parameters.add(lootTotal);
                
                DatabaseUtils.createSubmission(dbConnection, parameters);
                
                //find shortest array
                int shortestArray = characterNames.length;
                if(shares.length < shortestArray){
                    shortestArray = shares.length;
                }
                if(lootCut.length < shortestArray){
                    shortestArray = lootCut.length;
                }
                
                //fill the arrayList with parameters for the payouts table
                if(shortestArray > 0){
                    parameters = new ArrayList<Object>();
                    parameters.add(shares[0]);
                    parameters.add(transactionID);
                    parameters.add(characterNames[0]);
                    for(int i = 1; i < shortestArray; i++){
                        parameters.add(shares[i]);
                        parameters.add(transactionID);
                        parameters.add(characterNames[i]);
                    }
                }
                DatabaseUtils.createPayouts(dbConnection, parameters, shortestArray);
                
                
                dbConnection.commit();
            }catch(Exception ex){
                ex.printStackTrace();
            }finally{
                DatabaseUtils.close(dbConnection);
            }
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        response.sendRedirect("/WebLootCut/TransactionDisplay?transactionID=" + transactionID);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
