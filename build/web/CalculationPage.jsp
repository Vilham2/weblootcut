<%-- 
    Document   : CalculationPage
    Created on : Mar 10, 2017, 9:55:10 AM
    Author     : Student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculation Page</title>
        
    <div style="display: none;">
        <div id="containerElement">
                Character Name: <input type="text" name="characterName" id="characterName">
                Shares: <input type="text" value="1" onkeyup="recalculate()" name="shares" id="shares">
                Loot Cut: <input type="text" name="lootCut" id="lootCut" readonly>
        </div>
    </div>
    </head>
    <body>
        <h1>Calculaton Page</h1>
        <form method="post" action="/WebLootCut/SubmitTransaction">
            <p>
                Loot Total: <input type="text" id="lootTotal" name="lootTotal" onkeyup="recalculate()">
                Tax Rate: <input type="text" id="taxRate" name="taxRate" value="7.5" onkeyup="recalculate()">%
            </p>
            <p>
                Corp Takes: <input type="text" id="corpCut" name="corpCut" readonly>
            </p>
            <p>
                <input type="button" value="Add Character" id="addCharacterButton" onclick="addCharacter()">
                <input type="button" value="Remove Character" onclick="removeCharacter()">
                <input type="submit" value="Submit">
            </p>

            <div id="container_div">

            </div>
        </form>
        
        
        
        <script>
            var lootTotalElement = document.getElementById("lootTotal");
            var corpCutElement = document.getElementById("corpCut");
            var taxRateElement = document.getElementById("taxRate");
            var originalElement = document.getElementById("containerElement");
            var containerDiv = document.getElementById("container_div");
            
            function recalculate(){
                var lootTotal = parseFloat(lootTotalElement.value);
                var taxRate = parseFloat(taxRateElement.value)/100;
                var corpCut = lootTotal * taxRate;
                corpCutElement.value = corpCut;
                var splitableLootTotal = lootTotal - corpCut;
                
                var characterList = containerDiv.childNodes;
                var characterCount = characterList.length;
                console.log("Detected " + characterCount + " characters");
                var shareTotal = 0;
                var i;
                for(i = 0; i < characterCount; i++){
                    var currentCharacter = characterList[i];
                    var subElements = currentCharacter.childNodes;
                    var length = subElements.length;
                    if(length === originalElement.childNodes.length){
                        //the location of the shares text field
                        var share = parseFloat(subElements[3].value);
                        console.log("current share value = " + share);
                        shareTotal = shareTotal + share;
                    }
                }
                //finished summing up share totals
                //begin divvying up loot
                for(i = 0; i < characterCount; i++){
                    var currentCharacter = characterList[i];
                    var subElements = currentCharacter.childNodes;
                    var length = subElements.length;
                    if(length === 7){
                        var share = parseFloat(subElements[3].value);
                        //the location of the loot cut text field
                        subElements[5].value = ((splitableLootTotal * share) / shareTotal).toFixed(2);
                    }
                    
                }
                
            }
            
            function addCharacter(){
                var elementClone = originalElement.cloneNode(true);
                elementClone.id = "";
                containerDiv.appendChild(elementClone);
                recalculate();
            }
            
            
            function removeCharacter(){
                containerDiv.removeChild(containerDiv.lastChild);
                recalculate();
            }
            
            addCharacter();
            addCharacter();
            
        </script>
        
    </body>
</html>
